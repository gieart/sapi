// Style Switcher Open/Close
$("#color-option .panel-button").click(function() {
    $("#color-option").toggleClass("close-color-option", "open-color-option", 1000);
    $("#color-option").toggleClass("open-color-option", "close-color-option", 1000);
    return false;
});

// Color Skins
$('.switcher').click(function() {
    var title = jQuery(this).attr('title');
    jQuery('#color-options').attr('href', 'assets/main/css/' + title + '.min.css');
    return false;
});
