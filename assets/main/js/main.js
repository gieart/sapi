
$(document).ready(function() {
    /* affix the navbar after scroll below header */
    //var pixel = $('header').height() - $('#nav').height();
    $('#navbar').affix({});

    /* highlight the top nav as scrolling occurs */
    $('body').scrollspy({target: '#navbar'})

    /* smooth scrolling for scroll to top */
    $('.scroll-top').click(function() {
        $('body,html').animate({scrollTop: 0}, 1000);
    })

    /* smooth scrolling for nav sections */
    $('#navbar .navbar-nav li>a').click(function() {
        var link = $(this).attr('href');
//                    var posi = $(link).offset().top + 20;
        var posi = $(link).offset().top + 30;
        $('body,html').animate({scrollTop: posi}, 700);
    })

    wow = new WOW(
            {
                animateClass: 'animated',
                offset: 100
            }
    );
    wow.init();
});